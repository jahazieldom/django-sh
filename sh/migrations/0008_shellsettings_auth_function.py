# Generated by Django 3.2.7 on 2021-09-30 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sh', '0007_auto_20210921_2202'),
    ]

    operations = [
        migrations.AddField(
            model_name='shellsettings',
            name='auth_function',
            field=models.TextField(default='def is_valid_user(request):\n    """Authentication logic for every request of the django-sh app.\n    Don\'t change the name of the function!!\n\n    Parameters:\n    request (Request): Django request object\n\n    Returns:\n    bool: Continue or not the execution\n    """\n    return request.user.is_superuser\n', null=True),
        ),
    ]
