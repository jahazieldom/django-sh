#!/usr/bin/env bash

set -e

function echo_divider {
    echo "************************************"
}

function upload_pypi {
    echo_divider
    echo "Uploading to pypi..."
    echo_divider

    FILE="$HOME/.pypirc"
    if [ -f "$FILE" ]; then
        echo "$FILE found."
    else 
        cp ".pypirc" "$FILE"
        echo "$FILE created."
    fi

    python3 -m twine upload dist/*
}


function build {
    echo_divider
    echo "Building..."
    echo_divider
    rm -rf dist/ build/ django_cfdi.egg-info/
    python3 setup.py bdist_wheel
}

build
upload_pypi
